module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 1 = 1.0
hw1_2 n = 1.0 / fromIntegral(n ^ n) + hw1_2 ( n - 1 )

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n | n >= 2 = n * fact2 (n - 2) 
        | otherwise = 1

-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isPrime :: Integer -> Bool
isPrime 1 = False
isPrime 2 = True
isPrime p | p <= 0 = False
          | p `mod` 2 == 0 = False
          | otherwise = check p 3
		  
check p x | p < x * x = True
          | p `mod` x == 0 = False
          | otherwise = check p (x + 2)		  

-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b | a > b = 0
					| a == b = if (isPrime (a) ) then a else 0
                    | isPrime(a) == True = a + primeSum (a + 1) (b)
                 |otherwise = primeSum (a + 1) (b)
