-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown x = case x of
	On -> True
	Off -> True
	Unknown -> False

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval term = case term of
	Const x -> x
	Mult a b -> eval a * eval b
	Add a b -> eval a + eval b
	Sub a b -> eval a - eval b
	
-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Const x) = Const x
simplify (Add a b) = Add (simplify a) (simplify b)
simplify (Mult a b) = Mult (simplify a) (simplify b)
simplify (Sub a b) = Sub (simplify a) (simplify b)
simplify (Mult (Add a b) (Add c d)) = simplify (Add (Add (Mult (simplify a) (simplify c)) (Mult (simplify a) (simplify d))) (Add (Mult (simplify b) (simplify c)) (Mult (simplify b) (simplify d))))
simplify (Mult (Add a b) c) = simplify (Add (Mult (simplify a) (simplify c)) (Mult (simplify b) (simplify c)))
simplify (Mult a (Add b c)) = simplify (Add (Mult (simplify a) (simplify b)) (Mult (simplify a) (simplify c)))
simplify (Mult (Sub a b) c) = simplify (Sub (Mult (simplify a) (simplify c)) (Mult (simplify b) (simplify c)))
simplify (Mult a (Sub b c)) = simplify (Sub (Mult (simplify a) (simplify b)) (Mult (simplify a) (simplify c)))

